{-# LANGUAGE OverloadedStrings #-}
module Main where

import XpathParser (parseXpathSimple)

main :: IO ()
main = do
  let r = parseXpathSimple "/a"
  print r
