{-# LANGUAGE OverloadedStrings #-}
import Test.Tasty
import Test.Tasty.HUnit

import Data.List
import Data.Ord
import Data.Either
import qualified Data.Text                as T

import MarkupParserSpec (markupUnitTests)
import XpathParserSpec (xpathUnitTests)
import XpathEvalSpec (xpathEvalUnitTests)
import XpathTypes

main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [markupUnitTests, xpathUnitTests, xpathEvalUnitTests]

