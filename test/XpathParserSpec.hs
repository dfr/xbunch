{-# LANGUAGE OverloadedStrings #-}
module XpathParserSpec (xpathUnitTests) where

import Test.Tasty
import Test.Tasty.HUnit

import Data.List
import Data.Ord
import Data.Either
import qualified Data.Text                as T

import XpathParser (parseXpathSimple)
import XpathTypes

xpathUnitTests = testGroup "XpathParserSpec tests"
  [ testCase "Complex1" $
    parse "ancestor::*[count(child::*) > 1]/*[not(. = current()/ancestor-or-self::*)]" @=?
    PathConst Rel Nothing [LocStep {axis = Ancestor, nodeTest = NodeNamed "*", predicates = [BinOp Gt (FuncConst "count" [PathConst Rel Nothing [LocStep {axis = Child, nodeTest = NodeNamed "*", predicates = []}]]) (NumConst 1.0)]},LocStep {axis = Child, nodeTest = NodeNamed "*", predicates = [FuncConst "not" [BinOp Eql (PathConst Rel Nothing [LocStep {axis = Self, nodeTest = NodeNode, predicates = []}]) (PathConst Rel (Just (FuncConst "current" [])) [LocStep {axis = AncestorOrSelf, nodeTest = NodeNamed "*", predicates = []}])]]}]

  , testCase "Simple tag abs" $
      parse "/a" @=?
      PathConst Abs Nothing [LocStep {axis = Child, nodeTest = NodeNamed "a", predicates = []}]

  , testCase "Simple tag rel" $
      parse "a" @=?
      PathConst Rel Nothing [LocStep {axis = Child, nodeTest = NodeNamed "a", predicates = []}]

  , testCase "Simple tag abs descedant" $
      parse "//a" @=?
      PathConst Abs Nothing [LocStep {axis = DescendantOrSelf, nodeTest = NodeNode, predicates = []},LocStep {axis = Child, nodeTest = NodeNamed "a", predicates = []}]
  ]

parse :: T.Text -> Expr
parse text =
      case parseXpathSimple text of
        (Right r) -> r
        (Left l) -> error l
