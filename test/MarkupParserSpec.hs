{-# LANGUAGE OverloadedStrings #-}
module MarkupParserSpec (markupUnitTests) where

import Test.Tasty
import Test.Tasty.HUnit

import Data.List
import Data.Ord
import Data.Either
import qualified Data.Text                as T

import MarkupParser (parseMarkup, Elem(..))

markupUnitTests = testGroup "MarkupParserSpec tests"
  [ testCase "Simple tag" $
    parse "<html></html>" @=? RootTag [Tag {tagName = "html", tagAttrs = [], tagChildren = []}]

  , testCase "Tag with attr" $
      parse "<html lang=\"es\"></html>" @=?
      RootTag [Tag {tagName = "html", tagAttrs = [("lang","es")], tagChildren = []}]

  , testCase "Noncloseable tag" $
      parse "<html><img></html>" @=?
      RootTag [Tag {tagName = "html", tagAttrs = [], tagChildren = [Tag {tagName = "img", tagAttrs = [], tagChildren = []}]}]

  , testCase "Script tag" $
      parse "<script>var a = 1 < 2;</script>" @=?
      RootTag [Tag {tagName = "script", tagAttrs = [], tagChildren = [TagText "var a = 1 < 2;"]}]

  , testCase "Script tag1" $
      parse "<script>var a = '</b>';</script>" @=?
      RootTag [Tag {tagName = "script", tagAttrs = [], tagChildren = [TagText "var a = '</b>';"]}]

  , testCase "With headers and whitespaces" $
      parse "<!DOCTYPE html>\n  <html>\n  </html>\n  \n" @=?
      RootTag [Tag {tagName = "html", tagAttrs = [], tagChildren = []}]
  ]

parse :: T.Text -> Elem
parse text =
  let (Right r) = parseMarkup text in r
