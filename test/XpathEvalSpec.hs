{-# LANGUAGE OverloadedStrings #-}
module XpathEvalSpec (xpathEvalUnitTests) where

import Test.Tasty
import Test.Tasty.HUnit

import Data.List
import Data.Ord
import Data.Either
import qualified Data.Text                as T

import XpathEval (findNodes)
import XpathTypes
import MarkupParser (Elem(..))


htmlEx1 :: T.Text
htmlEx1 = "<html><a href=''>kyky</a><p><a/></p></html>"

xpathEvalUnitTests = testGroup "XpathEvalSpec tests"
  [ testCase "Simple child" $
    (simpleFindNodes "html/p" htmlEx1) @=? [["html","p"]]
    , testCase "Simple tag" $
      (simpleFindNodes "html" htmlEx1) @=? [["html"]]
    , testCase "Simple slash tag" $
      (simpleFindNodes "html" htmlEx1) @=? [["html"]]
    , testCase "Simple dot slash tag" $
      (simpleFindNodes "./html" htmlEx1) @=? [["html"]]
    , testCase "Simple node" $
      (simpleFindNodes "html//node()" htmlEx1) @=? [["html"],["html","a"],["html","p"],["html","p","a"]]
  ]

simplifyFoundTag :: [[Elem]] -> [[T.Text]]
simplifyFoundTag = fmap (fmap s) where s (Tag n _ _) = n
                                       s _ = "ROOT"

simpleFindNodes ::  T.Text -> T.Text -> [[T.Text]]
simpleFindNodes x c = simplifyFoundTag $ fromRight (error "fromRight: got 'Left'") (findNodes x c)
