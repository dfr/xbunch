{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS -Wall #-}

module XpathEval (findNodes) where

-- import           Control.Monad (ap)
-- import           Data.Monoid              ((<>))
-- import           Control.Monad            (msum)
-- import           Control.Monad (ap)
-- import           Data.Monoid              ((<>))
-- import           Control.Monad            (msum)
-- import           Control.Monad (ap)
-- import           Data.Monoid              ((<>))
-- import           Control.Monad            (msum)
-- import           Control.Monad (ap)
-- import           Data.Monoid              ((<>))
-- import           Control.Monad            (msum)
import Control.Lens (Traversal', traverseOf, mapMOf)
import Control.Monad.State (State, get, put, modify, evalState, execState)
import           Debug.Trace              (traceShow,
                                           trace, traceStack, traceM, traceShowM)
-- import qualified Data.HashMap.Lazy as Map
import XpathParser (parseXpathSimple)
import qualified Data.Text                as T
import qualified XpathTypes               as X
import MarkupParser (parseMarkup, Elem(..))



data ElemType = TagType | RootTagType | CommentType | TagTextType | TagHdrType
  deriving (Eq, Read, Show)

data NavNode = NavNode { _ntType :: ElemType
                       , _ntLabel :: (Maybe T.Text)
                       , _ntIdx :: Int
                       , _ntAttrs :: [(T.Text, T.Text)] }
             deriving (Eq, Read, Show)

-- Main!


findNodes :: T.Text -> T.Text -> Either String [[Elem]]
findNodes xpathstr html =
  findNodes'
  <$> parseXpathSimple xpathstr
  <*> parseMarkup html

{- 

>>> fromRight' $ findNodes "//a" "<html><a href=''>kyky</a><p><a/></p></html>"
[[Tag {tagName = "html", tagAttrs = [], tagChildren = [Tag {tagName = "a", tagAttrs = [("href","")], tagChildren = [TagText "kyky"]},Tag {tagName = "p", tagAttrs = [], tagChildren = [Tag {tagName = "a", tagAttrs = [], tagChildren = []}]}]},Tag {tagName = "a", tagAttrs = [("href","")], tagChildren = [TagText "kyky"]}],[Tag {tagName = "html", tagAttrs = [], tagChildren = [Tag {tagName = "a", tagAttrs = [("href","")], tagChildren = [TagText "kyky"]},Tag {tagName = "p", tagAttrs = [], tagChildren = [Tag {tagName = "a", tagAttrs = [], tagChildren = []}]}]},Tag {tagName = "p", tagAttrs = [], tagChildren = [Tag {tagName = "a", tagAttrs = [], tagChildren = []}]},Tag {tagName = "a", tagAttrs = [], tagChildren = []}]]


>> parseXpathSimple "//a"
Right (PathConst Abs Nothing [
  LocStep {axis = DescendantOrSelf, nodeTest = NodeNode, predicates = []},
  LocStep {axis = Child, nodeTest = NodeNamed "a", predicates = []}
])

>> parseMarkup "<html><a href=''>kyky</a><p><a/></p></html>"
Right (RootTag [Tag {tagName = "html", tagAttrs = [], tagChildren = [
  Tag {tagName = "a", tagAttrs = [("href","")], tagChildren = [TagText "kyky"]},
  Tag {tagName = "p", tagAttrs = [], tagChildren = [
    Tag {tagName = "a", tagAttrs = [], tagChildren = []}]}
  ]}
])

-- TODO add filter by predicate
-}
-- go :: Applicative f => (a -> f b) -> X.Expr -> f X.Expr
traversalXExpr :: Traversal' X.Expr X.Expr
traversalXExpr f = \case
  X.PathConst ptype cond steps -> X.PathConst ptype <$> traverse f cond <*> pure steps
  X.FilterConst exs -> X.FilterConst <$> traverse f exs
  X.FuncConst txt exs -> X.FuncConst txt <$> traverse f exs
  X.BinOp bo ex ex' -> X.BinOp bo <$> f ex <*> f ex'
  X.UnOp uo ex -> X.UnOp uo <$> f ex
  x -> pure x



findNodesS :: X.Expr -> [[Elem]] -> [[Elem]]
findNodesS xexpr = execState (mapMOf traversalXExpr processNodeS xexpr)

processNodeS :: X.Expr -> State [[Elem]] X.Expr
processNodeS x = undefined


findNodes' :: X.Expr -> Elem -> [[Elem]]
findNodes' (X.PathConst _ _ steps) elemTree =
  let
    rsteps@(lastStep:_) = reverse steps
    elemList = findTagsInTreeByXpathNode elemTree (X._nodeTest lastStep)
    -- enrich with tracking struct
    -- [ROOT, html, div, p, a] -> ([html, div, p, a], [p, div, html])
    elemList' = [(e, reverse e) | (_root:e) <- elemList ]
    elemList'' = filterELs rsteps elemList'
  in map fst elemList''
  where
    -- [ a, ... ] a -> True
    isTag (Tag _ _ _) = True
    isTag _ = False
    hasNearestTag ((Tag tName _ _):_) n = tName == n
    hasNearestTag _ _ = False
    -- [ a, div, p ] a -> True
    hasAnyTag ts n = any (\(Tag tName _ _) -> tName == n) ts
    -- [ x'a', x'//' ] [[a,p,div], [b,div]] -> [a,p,div]
    filterELs (_curStep:axsStep:parntStep:rsteps) elemList = case X._axis axsStep of
      X.DescendantOrSelf ->
        case X._nodeTest parntStep of
          X.NodeNamed nn ->
            -- check if there is any such parent tag and also do cutoff
            -- tracking struct: for nn=div: [p, div, html] -> [html]
            let els' = [ (e, tail tre') | (e, tre) <- elemList, let tre' = dropWhile (\(Tag tn _ _) -> tn /= nn) tre, length tre' > 0 ]
            in traceShow ("01" :: T.Text) $ filterELs rsteps els'
          _ -> traceShow ("1" :: T.Text) $ traceShow axsStep elemList
      _ ->
        let (X.NodeNamed nn) = (X._nodeTest axsStep)
            -- TODO factor out matched elem!
            els' = filter (\(_eos, es) -> hasNearestTag es nn) elemList
        in traceShow ("2" :: T.Text) $ traceShow axsStep $ filterELs (parntStep:rsteps) els'
    filterELs (_curStep:axsStep:[]) elemList = case (X._axis axsStep) of
       X.DescendantOrSelf -> elemList
       _ -> traceShow ("3" :: T.Text) $ traceShow axsStep elemList
    filterELs [curStep] elemList =
      case X._nodeTest curStep of
        X.NodeNamed nn ->
          let elemList' = [ (e, tre) | (e, tre) <- elemList, let tre' = dropWhile (\(Tag tn _ _) -> tn /= nn) $ tre, length tre' == 1 ]
          in traceShow ("4" :: T.Text) elemList'
        _ -> traceShow ("5" :: T.Text) elemList
    filterELs [] elemList = traceShow ("6" :: T.Text) elemList

findNodes' _ _ = error "findNodes only for PathConst"

-- findTagsInTreeByXpathNode :: Elem -> NodeExpr -> [[Elem]]


elemTreeToElemList :: Elem -> [[Elem]]
elemTreeToElemList element = find [] element
  where
    find :: [Elem] -> Elem -> [[Elem]]
    find path el =
      let
          rest = (find (path ++ [el])) =<< children
          me = case el of
            Tag n _ _ -> path ++ [el]
            _ -> []
          children = case el of
            Tag _ _ ch -> ch
            RootTag ch -> ch
            _ -> []
      in if null me then rest else [me] ++ rest


findTagsInTreeByXpathNode :: Elem -> X.NodeExpr -> [[Elem]]
findTagsInTreeByXpathNode element nodeExpr = find [] element
  where
    find :: [Elem] -> Elem -> [[Elem]]
    find path el =
      let
          tagname = case nodeExpr of
            X.NodeNamed n -> n;
            _ -> ""
          rest = (find (path ++ [el])) =<< children
          me = case el of
            Tag n _ _ | tagname == "" || n == tagname -> path ++ [el]
            _ -> []
          children = case el of
            Tag _ _ ch -> ch
            RootTag ch -> ch
            _ -> []
      in if null me then rest else [me] ++ rest


testFindNodes1 :: T.Text
testFindNodes1 =
  let elems = fromRight' $ findNodes "//a" "<html><a href=''>kyky</a><p><a/></p></html>"
      m = elems == []
      in if m then "OK" else T.pack $ show elems


-- exprEval :: Expr -> Elem -> Bool
-- exprEval (PathConst _ _ locsteps) nt = undefined
-- exprEval (NumConst n) nt = ntLabel nt
-- exprEval (StrConst s) nt = undefined
-- exprEval (StrConst s) nt = undefined
-- exprEval (FuncConst fname args) nt = undefined
-- exprEval (VarConst t) nt = undefined
-- exprEval (BinOp op e1 e2) e = undefined
-- exprEval (UnOp op e1) e = undefined



filterNamed :: T.Text -> Elem -> [Elem]
filterNamed n t@(Tag tagname _ tagchld) | n == tagname =
                                            t : (filterNamed n =<< tagchld)
                                        | otherwise =
                                            filterNamed n =<< tagchld
filterNamed n (RootTag tagchld) = filterNamed n =<< tagchld
filterNamed _ _ = []


fromRight'           :: Either a b -> b
fromRight' (Left _)  = error "fromRight': got 'Left'"
fromRight' (Right x) = x

-- filterMarkup :: (Elem -> Bool) -> Elem -> Elem
-- filterMarkup f (Tag n a c) = f (Tag (f n) (f a) (f c))

{-

testMarkup :: T.Text
testMarkup =  "<html><head> \n\
  \ <meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1251\"> \n\
  \ <link rel=\"shortcut icon\" href=\"/favicon.ico\"> \n\
  \ <!--[if lte IE 8]> \n\
  \ <link rel=\"stylesheet\" type=\"text/css\" href=\"/_all/top/ie8.css?nocache=20140901\"> \n\
  \ <![endif]--> \n\
  \ <script type=\"text/javascript\" src=\"/_/js/jquery.min.js\" ></script> \n\
  \ </head> \n\
  \ <body> \n\
  \ <div id=\"test1\"></div> \n\
  \ </body></html>"


-}

markup1 :: T.Text
markup1 = "<html><a href=''>kyky</a><p><a/><a/></p><a/></html>"

simplifyFoundTag :: [[Elem]] -> [[T.Text]]
simplifyFoundTag = fmap (fmap s) where s (Tag n _ _) = n
                                       s _ = "ROOT"

testFindTagsInTreeByName1 :: Bool
testFindTagsInTreeByName1 =
  let Right m = parseMarkup markup1
      r = simplifyFoundTag $ findTagsInTreeByXpathNode m (X.NodeNamed "a")
  in r == [["ROOT","html","a"],["ROOT","html","p","a"],["ROOT","html","p","a"],["ROOT","html","a"]]

testFindTagsInTreeByName2 :: Bool
testFindTagsInTreeByName2 =
  let Right m = parseMarkup markup1
      r = simplifyFoundTag $ findTagsInTreeByXpathNode m X.NodeAny
  in r == [["ROOT","html"],["ROOT","html","a"],["ROOT","html","p"],["ROOT","html","p","a"],["ROOT","html","p","a"],["ROOT","html","a"]]

