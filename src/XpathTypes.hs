{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveFunctor     #-}
{-# LANGUAGE DeriveTraversable #-}
{-# OPTIONS -Wall #-}
module XpathTypes where

import qualified Data.Text                as T
import MarkupParser (Elem)

data Axis =
  Ancestor
  | AncestorOrSelf
  | Attribute
  | Child
  | Descendant
  | DescendantOrSelf
  | Following
  | FollowingSibling
  | Namespace
  | Parent
  | Preceding
  | PrecedingSibling
  | Self
  deriving (Show, Eq)

data BinOperator = Add | Sub | Mult | Div | Mod | Lt | Gt | Lte | Gte | Eql | Neql | Uni
                 | Or | And
                 deriving (Show, Eq)

data UnOperator = Neg
                deriving (Show, Eq)

data PathType = Abs | Rel
              deriving (Show, Eq)

data Expr =
  NumConst Double
  | StrConst T.Text
  | PathConst PathType (Maybe (Expr)) [LocStep] 
  | FilterConst [Expr]
  | FuncConst T.Text [Expr]
  | VarConst T.Text
  | BinOp BinOperator (Expr) (Expr)
  | UnOp UnOperator (Expr)
    deriving (Show, Eq)

data NodeExpr =
  NodeAny -- node()
  | NodeNamed T.Text
  | NodeComment
  | NodeText
  | NodePI
    deriving (Show, Eq)

--
-- axis based predicate semantics
data LocStep = LocStep { _axis :: Axis
                       , _nodeTest :: NodeExpr
                       , _predicates :: [Expr]
                       }
             deriving (Show, Eq)

