{-# LANGUAGE OverloadedStrings, BangPatterns #-}
{-# OPTIONS -Wall #-}

module XpathParser (parseXpathSimple, xpathP) where

import           Control.Applicative
import           Control.Monad (when)
import           Data.Char                (isAlpha, isAlphaNum)
import           Data.Monoid              ((<>))
import qualified Data.Attoparsec.Text     as P
import qualified Data.Text                as T
import           Debug.Trace              (traceShow)
--                                           trace, traceStack, traceM, traceShowM)
-- import Text.XML.HXT.XPath.XPathEval (parseXPathExpr)
import XpathTypes


-- ...Таким образом, оси ancestor, ancestor-or-self, preceding и preceding-sibling являются обратными осями, а все остальные - прямыми.
-- ...Фактически, child используется как ось по умолчанию. Например, путь адресации div/para становится сокращением для child::div/child::para.
-- Аналогичные аббревиатуры имеются и для атрибутов:
-- attribute:: может быть сокращен до @.
-- Например, путь адресации para[@type="warning"]
-- является сокращением для child::para[attribute::type="warning"]
-- // является сокращением для /descendant-or-self::node()/. Например, //para - это сокращение для /descendant-or-self::node()/child::para
-- Шаг адресации . является сокращением для self::node()
-- Точно так же, шаг адресации .. является сокращением для parent::node()

infixr 0 <??>

(<??>) :: String -> P.Parser a -> P.Parser a
(<??>) name p = p P.<?> traceShow name name
-- _ <??> p = p

childStep :: LocStep
childStep = LocStep Child NodeAny []

desctStep :: LocStep
desctStep = LocStep DescendantOrSelf NodeAny []

xpathP :: P.Parser Expr
xpathP = exprP

exprP :: P.Parser Expr
exprP = "expr" <??> orExprP

orExprP :: P.Parser Expr
orExprP = "orExpr" <??>
  f <$> andExprP <*> P.many' (wsStringP "or" *> andExprP)
  where
    f e1 [] = e1
    f e1 (e2:es) = BinOp Or e1 (f e2 es)

andExprP :: P.Parser Expr
andExprP = "andExpr" <??>
  f <$> equalityExprP <*> P.many' (wsStringP "and" *> equalityExprP)
  where
    f e1 [] = e1
    f e1 (e2:es) = BinOp And e1 (f e2 es)

equalityExprP :: P.Parser Expr
equalityExprP = "equalityExpr" <??>
  f <$> relationalExprP <*> P.many' ((,) <$> wsP ("=" <|> "!=") <*> relationalExprP)
  where
    f e [] = e
    f e1 ((op, e2):rs) = case op of
                       "=" -> BinOp Eql e1 (f e2 rs)
                       "!=" -> BinOp Neql e1 (f e2 rs)
                       _ -> undefined
    
relationalExprP :: P.Parser Expr
relationalExprP = "relationalExpr" <??>
  f <$> additiveExprP <*> P.many' ((,) <$> wsP (P.choice ["<",">","<=",">="]) <*> additiveExprP)
  where
    f e [] = e
    f e1 ((op, e2):rs) =
      case op of
        "<" -> BinOp Lt e1 (f e2 rs)
        ">" -> BinOp Gt e1 (f e2 rs)
        "<=" -> BinOp Lte e1 (f e2 rs)
        ">=" -> BinOp Gte e1 (f e2 rs)
        _ -> undefined

multiplicativeExprP :: P.Parser Expr
multiplicativeExprP = "multiplicativeExpr" <??>
  f <$> unaryExprNoRootP <*> (P.option [] ((\op e -> [(op,e)]) <$> wsP ("*" <|> "div" <|> "mod") <*> multiplicativeExprP))
  <|> (\rs -> f (PathConst Abs Nothing [childStep]) rs) <$> (wsP "/" *> P.option [] ((\op e -> [(op,e)]) <$> wsP ("div" <|> "mod") <*> multiplicativeExprP))
  where
    f :: Expr -> [(T.Text, Expr)] -> Expr
    f e [] = e
    f e1 ((op, e2):rs) = case op of
                       "*" -> BinOp Mult e1 (f e2 rs)
                       "div" -> BinOp Div e1 (f e2 rs)
                       "mod" -> BinOp Mod e1 (f e2 rs)
                       _ -> undefined

additiveExprP :: P.Parser Expr
additiveExprP = "additiveExpr" <??>
  f <$> multiplicativeExprP <*> P.many' ((,) <$> wsP ("+" <|> "-") <*> multiplicativeExprP)
  where
    f e [] = e
    f e1 ((op, e2):rs) = case op of
                       "+" -> BinOp Add e1 (f e2 rs)
                       "-" -> BinOp Sub e1 (f e2 rs)
                       _ -> undefined

unaryExprNoRootP :: P.Parser Expr
unaryExprNoRootP = "unaryExpr" <??>
  f <$> (P.many' (wsP "-")) <*> unionExprNoRootP
  where
    f neg e = if even (length neg) then e else UnOp Neg e

unionExprNoRootP :: P.Parser Expr
unionExprNoRootP = "unionExprNoRootP" <??>
  funion1 <$> pathExprNoRootP <*> P.option [] ((:[]) <$> (wsStringP "|" *> unionExprNoRootP))
  <|> (\e -> BinOp Uni (PathConst Abs Nothing [childStep]) e) <$> (wsP "/" *> wsP "|" *> unionExprNoRootP)
  <|> pathExprNoRootP
  where
    funion1 e [] = e
    funion1 e1 (e2:_) = BinOp Uni e1 e2

pathExprNoRootP :: P.Parser Expr
pathExprNoRootP = "pathExprNoRootP" <??>
  f <$> filterExprP <*> P.option Nothing ((\a b -> Just (a,b)) <$> (wsP "//" <|> wsP "/") <*> relLocPathP)
  <|> locPathP
  where
    f flt Nothing = flt
    f flt (Just (slash,PathConst t _ steps)) =
      if slash == "/"
      then PathConst t (Just flt) steps
      else PathConst t (Just flt) (desctStep:steps)
    f _ _ = undefined

locPathP :: P.Parser Expr
locPathP = "locPath" <??> relLocPathP <|> absLocPathNoRootP

filterExprP :: P.Parser Expr
filterExprP = "filterExpr" <??>
  ffilter <$> primaryExprP <*> (P.many' predicateP)
  where
    ffilter ::  Expr ->  [Expr] -> Expr
    ffilter e [] = e
    ffilter e xs = FilterConst (e:xs)

primaryExprP :: P.Parser Expr
primaryExprP = "primaryExpr" <??>
  VarConst <$> variableRefP
  <|> "(" *> exprP <* ")"
  <|> StrConst <$> literalP
  <|> NumConst <$> P.double
  <|> funCallP

variableRefP :: P.Parser T.Text
variableRefP = "variableRef" <??> T.cons <$> P.char '$' <*> qNameP

absLocPathNoRootP :: P.Parser Expr
absLocPathNoRootP = "absLocPath" <??>
  (\(PathConst _ e s) -> PathConst Abs e s) <$> ("/" *> relLocPathP)
  <|> (\(PathConst _ e s) -> PathConst Abs e (desctStep:s)) <$> ("//" *> relLocPathP)


relLocPathP :: P.Parser Expr
relLocPathP = "relLocPath" <??>
  (\s rs -> PathConst Rel Nothing (s:rs)) <$> stepP <*> (concat <$> P.many' (slsteps <$> ("//" <|> "/") <*> stepP))
  where
    slsteps s p = if s == "/"
                 then [p]
                 else desctStep : [p]

stepP :: P.Parser LocStep
stepP = "step" <??>
   (\a n p -> LocStep a n p) <$> axisSpecifierP <*> nodeTestP <*> (P.many' predicateP)
   <|> abbrStepP

axisSpecifierP :: P.Parser Axis
axisSpecifierP = "axisSpecifier" <??>
  (axisNameP <* "::") <|>  P.option Child (const Attribute <$> "@")

axisNameP :: P.Parser Axis
axisNameP = "axisname" <??>
  const AncestorOrSelf <$> P.string "ancestor-or-self"
  <|> const Ancestor <$> P.string "ancestor"
  <|> const Attribute <$> P.string "attribute"
  <|> const Child <$> P.string "child"
  <|> const DescendantOrSelf <$> P.string "descendant-or-self"
  <|> const Descendant <$> P.string "descendant"
  <|> const FollowingSibling <$> P.string "following-sibling"
  <|> const Following <$> P.string "following"
  <|> const Namespace <$> P.string "namespace"
  <|> const Parent <$> P.string "parent"
  <|> const PrecedingSibling <$> P.string "preceding-sibling"
  <|> const Preceding <$> P.string "preceding"
  <|> const Self <$> P.string "self"


nodeTestP :: P.Parser NodeExpr
nodeTestP = "nodetest" <??>
  nodeTypeP <* "()" <|> NodeNamed <$> nameTestP


predicateP :: P.Parser Expr
predicateP = "predicate" <??> "[" *> exprP <* "]"

abbrStepP :: P.Parser LocStep
abbrStepP = "abbrStep" <??>
  (\_ -> LocStep Self NodeAny []) <$> P.string "."
  <|> (\_ -> LocStep Parent NodeAny []) <$> P.string ".."

funCallP :: P.Parser Expr
funCallP = "funCall" <??>
  (\funName _ args _ -> FuncConst funName args)
  <$> funNameP <*> "(" <*> argListP <*> ")"

argListP :: P.Parser [Expr]
argListP = "argList" <??>
  P.sepBy exprP (P.char ',')

-- qName  :  nCName (':' nCName)?
qNameP :: P.Parser T.Text
qNameP = (<>) <$> ncNameP <*> P.option "" ((<>) <$> ":" <*> ncNameP)

ncNameP :: P.Parser T.Text
ncNameP = "ncName" <??> do
  part1 <- P.takeWhile (\c -> isAlpha c && c /= ':')
  part2 <- P.takeWhile (\c -> c /= ':' && (isAlphaNum c || c == '-' || c == '.'))
  when (T.null part1) $ fail "bad node name"
  return (part1 <> part2)

-- functionName
--  :  qName  // Does not match nodeType, as per spec.
funNameP :: P.Parser T.Text
funNameP = "funName" <??> qNameP


nameTestP :: P.Parser T.Text
nameTestP = "nameTest" <??>
  wsP "*"
  <|> (\a b c -> a<>b<>c) <$> ncNameP <*> ":" <*> "*"
  <|> qNameP


literalP :: P.Parser T.Text
literalP = "literal" <??>
  P.char '"' *> P.takeWhile (/= '"') <* P.char '"'
  <|> P.char '\'' *> P.takeWhile (/= '\'') <* P.char '\''

nodeTypeP :: P.Parser NodeExpr
nodeTypeP = "nodeType" <??>
  const NodeComment <$> P.string "comment"
  <|> const NodeText <$> P.string "text"
  <|> const NodePI <$> P.string "processing-instruction"
  <|> const NodeAny <$> P.string "node"


{-
-- http://blog.jwbroek.com/2010/07/antlr-grammar-for-parsing-xpath-10.html

-- xml name without ':'
ncNameStartCharP :: P.Parser Char
ncNameStartCharP = P.satisfy (P.inClass "_A-Za-z\xC0-\xD6\xD8-\xF6\xF8-\x2FF\x370-\x37D\x37F-\x1FFF\x200C-\x200D\x2070-\x218F\x2C00-\x2FEF\x3001-\xD7FF\xF900-\xFDCF\xFDF0-\xFFFD\x10000-\xEFFFF")
ncNameCharP :: P.Parser Char
ncNameCharP = ncNameStartCharP <|> P.satisfy (P.inClass "-.0-9\xB7\x0300-\x036F\x203F-\x2040")

-- simplified parser for name

ncNameP' :: P.Parser T.Text
ncNameP' = do
  part1 <- ncNameStartCharP
  part2 <- P.many' ncNameCharP
  let name = T.pack (part1 : part2)
  if T.null name then fail "nametoken" else return name

-}

wsStringP :: T.Text -> P.Parser T.Text
wsStringP s = id <$> P.skipSpace *> P.string s <* P.skipSpace

wsP :: P.Parser a -> P.Parser a
wsP p = id <$> P.skipSpace *> p <* P.skipSpace


parseXpathSimple :: T.Text -> Either String Expr
parseXpathSimple = P.parseOnly xpathP

-- "//div[contains(@class,'c')]/a"
-- "preceding-sibling::foo[1]/@att1"
-- "(24 div 3 +2) div (40 div 8 -3)"
