{-# LANGUAGE OverloadedStrings, DeriveFoldable #-}
--{-# OPTIONS -Wall #-}

module MarkupParser (parseMarkup, Elem(..)) where

-- import AParser (satisfy, char, spaces, runParser)
import           Control.Applicative
import           Control.Monad
import qualified Data.Attoparsec.Text     as P
import Data.Attoparsec.Combinator (lookAhead)
import qualified Data.ByteString          as B
import           Data.Char                (isAlpha, isAlphaNum, isSpace)
import           Data.Monoid              ((<>))
import qualified Data.Text                as T
import           Data.Text.Encoding       (decodeUtf8With)
import           Data.Text.Encoding.Error (lenientDecode)
import           Debug.Trace              (traceM, traceShow, traceShowM,
                                           traceStack)
--import System.IO                (hClose, openBinaryFile,IOMode(..))
--import Control.Exception	(bracket, finally)
--notString :: T.Text -> Parser T.Text
--notString s = sequence $ map notChar (T.unpack s)

data Elem = Tag { tagName     :: T.Text
                , tagAttrs    :: [(T.Text, T.Text)]
                , tagChildren :: [Elem] }
            | RootTag [Elem]
            | Comment T.Text
            | TagText T.Text
            | TagHdr (Maybe T.Text)
            deriving (Eq, Read, Show)

-- instance Show Elem where
--  show = T.unpack . showTree 0

nonClosedTags = ["link", "meta", "input", "img", "br"]

showTxt :: Int -> T.Text -> T.Text
showTxt depth = T.strip . T.replace "\n" ("\n" `T.append` indent depth)

indent :: Int -> T.Text
indent depth = T.replicate depth " "

showTree :: Int -> Elem -> T.Text
showTree depth (TagText txt) = T.concat [indent depth, showTxt depth txt, "\n" ]
showTree depth (Comment c) = T.concat [indent depth, "<!--", showTxt depth c, "-->" ]
showTree depth (Tag name attrs childs) = T.concat parts
  where
    parts = [indent depth, "<", name, attrsStr attrs ] ++ parts'
    parts' | name `elem` nonClosedTags = [ "/>\n"]
           | otherwise                 = [ ">\n",  childsStr, indent depth, "</", name, ">\n"]
    attrsStr [] = ""
    attrsStr  as = T.append " " (attrsStr' as)
    attrsStr' as = T.intercalate " " ((\(k,v) -> T.concat [k,"=\"",v,"\""]) <$> as)
    childsStr = T.intercalate "\n" (showTree (depth+1) <$> childs)


xmlhdr = xmlhdrP P.<?> "xmlhdr"

xmlhdrP = do
  P.skipSpace
  P.string "<?"
  hdr <- P.manyTill (P.notChar '?') (P.string "?>")
  P.skipSpace
  return hdr

doctype = doctypeP P.<?> "doctype"

doctypeP = do
  P.skipSpace
  P.string "<!DOCTYPE"
  dt <- P.takeWhile (/= '>')
  P.char '>'
  P.skipSpace
  return dt

nametoken = nametokenP P.<?> "nametoken"

nametokenP = do
  part1 <- P.takeWhile isAlpha
  part2 <- P.takeWhile (\c -> isAlphaNum c || c == '-' || c == '.')
  let name = T.append part1 part2
  if T.null name then fail "nametoken" else return name

tagattr = tagattrP P.<?> "tagattr"

tagattrP :: P.Parser (T.Text, T.Text)
tagattrP = do
  name <- nametoken
  val <- if T.null name then fail "" else  P.option "" tagattrval
  return (name, val)

tagattrval = tagattrvalP P.<?> "tagattrval"

tagattrvalP = do
  P.skipSpace
  P.skip (== '=')
  P.choice [quotedval, nametoken]

quotedval = quotedvalP P.<?> "quotedval"

quotedvalP = do
  q <- P.satisfy (\c -> c == '"' || c == '\'')
  val <- P.takeWhile (/= q)
  P.skip (== q)
  return val

closetag name = closetagP name P.<?> "closetag '" ++ T.unpack name ++ "'"

closetagP name = do
  P.char '<'
  P.char '/'
  n <- if name == ""
       then P.takeWhile (\c -> c /= '>' && not (isSpace c))
       else P.string name
  when (n == "") mzero
  P.skipSpace
  P.char '>'
  return n

textual :: T.Text -> P.Parser Elem
textual name = do
  text <- textual' name
  if T.null text then fail "textual" else return $ TagText text

textual' name = do
  text1 <- P.takeWhile (/= '<')
  if name `elem` ["script"]
    then do
    close <- lookAhead $ P.option "" (closetag "")
    text2 <- if close == name
             then return ""
             else T.cons <$> P.char '<' <*> textual' name
    return $ text1 <> text2
    else
    return text1


childtags :: T.Text -> P.Parser [Elem]
childtags name = childtagsP name P.<?> "childtags of '" ++ T.unpack name ++ "'"

childtagsP :: T.Text -> P.Parser [Elem]
childtagsP name = do
  ct <- P.sepBy (tagorcomment <|> textual name) P.skipSpace
  P.skipSpace
  closetag name
  return ct



comment :: P.Parser Elem
comment = commentP P.<?> "comment"


commentP :: P.Parser Elem
commentP = do
  P.skipSpace
  P.string "<!--"
  ctext <- T.pack <$> P.manyTill P.anyChar (P.string "-->" P.<?> "cc")
  return $ Comment ctext

tagelement :: P.Parser Elem
tagelement = tagelementP P.<?> "tagelement"

tagelementP :: P.Parser Elem
tagelementP = do
  P.skipSpace
  P.char '<'
  name <- nametoken
  P.skipSpace
  attrs <- P.sepBy tagattr P.skipSpace
  P.skipSpace
  selfClose <- P.option False (const True <$> P.char '/')
  --traceShowM (name, selfClose || name `elem` nonClosedTags)
  P.skipSpace
  P.char '>'
  P.skipSpace
  childtags <- if selfClose || name `elem` nonClosedTags then
                 return []
               else childtags name
  --p <- P.peekChar
  --when (name /= "") $ traceShowM $ ("</" <> name)
  return $ Tag name attrs childtags

tagorcomment :: P.Parser Elem
tagorcomment = tagorcommentP P.<?> "tagorcomment"

tagorcommentP :: P.Parser Elem
tagorcommentP =  tagelement <|> comment

markupParser :: P.Parser Elem
markupParser = do
  P.option "" xmlhdr
  P.option "" doctype
  tags <- P.sepBy tagorcomment P.skipSpace
  P.skipSpace
  return $ RootTag tags

parseMarkup :: T.Text -> Either String Elem
parseMarkup = P.parseOnly markupParser

parseFileSimple :: FilePath -> IO (Either String Elem)
parseFileSimple f = do
  bs <- B.readFile f
  let text = decodeUtf8With lenientDecode bs
  return $ parseMarkup text

parseFileWithPos :: FilePath -> IO ()
parseFileWithPos f = do
  (pos, r) <- step 0 $ P.parse markupParser T.empty
  case r of
    (P.Done _ res) -> print res
    (P.Fail _ contetxs str) -> print (pos, contetxs, str)
  where
    step :: Int -> P.Result a -> IO (Int,P.Result a)
    step pos (P.Partial k) = do
      (pos', text) <- src pos
      step pos' (k text)
    step pos r           = return (pos,r)
    src pos = do
        bs <- B.readFile f
        let text = decodeUtf8With lenientDecode bs
        let ff cnt _ = cnt+1
        let pos' = T.foldl ff pos text
        return (pos', text)

